import '../styles/slim.css'


function MyApp({ Component, pageProps }) {
  const currentYear = new Date().getFullYear()
  return <Component {...pageProps} />
}

export default MyApp


