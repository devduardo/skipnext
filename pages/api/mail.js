const mail = require('@sendgrid/mail');

mail.setApiKey(process.env.SENDGRID_API_KEY);


export default (req, res) => {
  const body = JSON.parse(req.body);

  const message = `
  Nombre Completo: ${body.customerName}\r\n
  Cedula - Pasaporte: ${body.customerId}\r\n
  Email: ${body.customerEmail}\r\n
  Telefono: ${body.customerPhone}\r\n
  Direccion: ${body.customerAddress}

  `;

const data = {
  to: 'skiplogistics@gmail.com',
  from: 'nuevo@skiplogistics.com',
  subject: 'Nuevo Registro de Cliente',
  text: message,
  html: message.replace(/\r\n/g, '<br>')
};

mail.send(data);

  res.status(200).json({ status: 'ok' })
}
