
import Head from 'next/head'
export default function Home() {
  async function handleOnSubmit(e){
    e.preventDefault();
    const formData = {};
    
    Array.from(e.currentTarget.elements).forEach(field =>{
      if (!field.name ) return;
      formData[field.name] = field.value;
    })
    fetch('/api/mail',{
    method: 'post',
    body: JSON.stringify(formData)
  })
//  console.log(formData);
  let currentLocation = window.origin;
 window.location.href = currentLocation;
  }
  return (
    <><div>
      <Head>
        <title>Skip Logistics | Carga Aerea y Maritima</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon"type="image/png" href="assets/images/favicon_skip.png"></link>
      </Head>
    </div>
    <div className="d-md-flex flex-row-reverse">
        <div className="signin-right">
          <div className="signin-box signup">
            <h3 className="signin-title-primary">Comencemos 🔥</h3>
            <h5 className="signin-title-secondary ">Llena los datos a continuación, verificaremos tus datos y te enviaremos un email con la confirmación de tu registro y dirección de casillero.</h5>
            <form method="post" onSubmit={handleOnSubmit}>
              <div className="row row-xs mg-b-10">
                <div className="col-sm"><input name="customerName" type="text" className="form-control" placeholder="Nombre Completo" required /></div>
                <div className="col-sm mg-t-10 mg-sm-t-0"><input name="customerid" type="text" className="form-control" placeholder="Cedula - Pasaporte" required /></div>
              </div>
              <div className="row row-xs mg-b-10">
                <div className="col-sm"><input name="customerEmail" type="email" className="form-control" placeholder="Email" required /></div>
                <div className="col-sm mg-t-10 mg-sm-t-0"><input name="customerPhone" type="number" className="form-control" placeholder="Teléfono" required /></div>
              </div>
              <div className="row row-xs mg-b-10">
                <div className="col-sm"><input name="customerAddress" type="text" className="form-control" placeholder="Dirección" required /></div>
              </div>
              <button className="btn  btn-purple btn-block">Crear Casillero 🚀</button>
            </form>
          </div>
        </div>
        <div className="signin-left">
          <div className="signin-box">
            <img src="assets/images/logo_skip.svg" className="w-50 mb-2" />
            <p>Somos una compañía dedicada a servicios de transporte de carga aerea y maritima, con mas de 300 destinos a nivel mundial. Actualmente estamos trabajando en nuestro nuevo portal web. por ahora podras registrar tu casillero y traer tus compras mientras acomodamos unas cosas por aca.</p>
            <p>Siguenos en instagram como @skiplogistics para mas informacion.</p>
            <p className="tx-12">&copy; Copyright 2021 Skip Logistics | All Rights Reserved.</p>
          </div>
        </div>
      </div></>
    
  )
}
